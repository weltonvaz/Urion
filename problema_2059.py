p, j1, j2, r, a = (int(x) for x in input().split())

soma = (j1 + j2)

if (soma % 2 == 0 and p == 1) or (soma % 2 == 1 and p == 0):
    win = 1
else:
    win = 2

if ((r == 1) and (a == 0)) or ((r == 0) and (a == 1)):
    win = 1
elif (r == 1 and a == 1):
    win = 2

print("Jogador %d ganha!" %win)
