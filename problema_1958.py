#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Números em ponto flutuante podem ser bastante extensos para mostrar. Nesses casos,
   é conveniente usar a notação científica.'''

print("%+.4E" %(float(input())))