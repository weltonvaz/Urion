#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Matheus e seu irmão gêmeo Vinicius adoram disputar para ver quem digita mais rápido.
Após muitos anos de duelo, eles chegaram à conclusão que o duelo nem sempre é vencido
por aquele que apenas digita mais rápido, pois outros fatores influenciam no ganhador.
Como cada um participa de sua própria casa, eles possuem um certo atraso para receber e
enviar os dados do servidor.'''

Am, Rm, Em = [int(x) for x in input().split()]
Av, Rv, Ev = [int(x) for x in input().split()]
S = input()

matheus = ((len(S)*Em) + (Am * 2) + Rm)
vinicius = ((len(S)*Ev) + (Av * 2) + Rv)

if matheus < vinicius:
    print('Matheus')
elif matheus > vinicius:
    print('Vinicius')
else:
    print('Empate')
