#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''As aulas do Prof. Jatobá estão dando o que falar. Os representantes do MEC vieram até a UNIME de Lauro de Freitas
para saber mais detalhes sobre essa nova forma de ensinar Algoritmos. Além disso, eles queriam selecionar 1 aluno para
participar da OBI-Tec (Olimpíada Brasileira de Informática Nível Técnica) e representar a rede Kroton na competição,
pois sabem que lá estão os melhores. Para selecionar o melhor, eles têm disponível uma lista com o número de inscrição
de cada aluno e a sua respectiva nota na disciplina. Sua tarefa é ajudar o pessoal do MEC a encontrar o aluno mais apto
a representar a instituição e quem sabe garantir sua vaga. Só tem um detalhe, se a nota mais alta não for maior ou igual
a 8, você deverá imprimir “Minimum note not reached”.'''

n = int(input())

wert = {}

for x in range(n):
    a, b = (x for x in input().split())
    wert[a] = float(b)

if wert[(max(wert, key=wert.get))] >= 8.0:
    print(max(wert, key=wert.get))
else:
    print("Minimum note not reached")