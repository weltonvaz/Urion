#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Finalmente, o time da Universidade conseguiu a classificação para a Final Nacional da Maratona de Programação da SBC.
Os três membros do time e o técnico estão ansiosos para bem representar a Universidade, e além de treinar muito, preparam
com todos os detalhes a sua viagem a São Paulo, onde será realizada a Final Nacional.'''

n = sum([int(x) for x in input().split()])
print(n - 3)