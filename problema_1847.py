#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""A entrada consiste apenas de três inteiros, A, B e C (-100 ≤ A, B, C ≤ 100), os quais representam respectivamente as
temperaturas registradas no 1º, no 2º e no 3º dias."""


wert = input()

a,b,c = wert.split()

m1 = int(b) - int(a)
m2 = int(c) - int(b)

if (m2 > m1):
    print(":)")
elif (m2 < m1):
    print(":(")
else:
    if (m1 <= 0):
        print(":(")
    else:
        print(":)")