#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Tri-du é um jogo de cartas derivado do popular jogo de Truco. O jogo utiliza um baralho
normal de 52 cartas, com treze cartas de cada naipe, mas os naipes são ignorados.
   Apenas o valor das cartas,considerados como inteiros de 1 a 13, são utilizados.'''

wert = [int(x) for x in input().split()]

if wert[0] > wert[1]:
    print(wert[0])
else:
    print(wert[1])