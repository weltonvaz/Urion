#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Dois amigos pedem ao atendente de uma lanchonete propor um desafio, de modo que quem acertasse mais,
não precisaria pagar a conta. Então foi proposto o seguinte: Dado o seguinte somatório abaixo, informar o resultado, com uma quantidade de termos no mesmo:'''

wert = int(input(""))

for x in range(wert):
    s = 0
    n = int(input(""))
    for i in range(n):
        if i % 2 == 0:
            s += 1
        else:
            s -= 1
    print(s)
    
