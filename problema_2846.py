#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''A sequência de Fibonot é composta pelos números que não pertencem à sequência de Fibonacci.
   Mais especificamente, os números inteiros positivos não-nulos. Em ordem crescente!
   Eis os primeiros termos de Fibonot: 4, 6, 7, 9, 10, 11, 12, 14, 15 ...
   Sua tarefa é achar o K-ésimo número de Fibonot.'''

fibo = [ 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025]

wert = int(input(''))
count = 0
for x in range(4,(10**6)+1):
    if (x not in fibo) == True:
        count += 1
        if count == wert:
            print(x)
            break
    