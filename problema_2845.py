#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Como o Grinch é tão ruim com números a ponto de nem saber que dois números só são chamados
de coprimos se o MDC (máximo divisor comum) entre eles é 1, Giovana simplesmente envia uma carta
para o polo norte com os números dos duendes que devem levar as comidas, e com isso, os duendes
já conseguem descobrir onde será a festa de aniversário, mas o Grinch não.'''

n = int(input())
wert = [int(x) for x in input().split()]

print((max(wert))+1)