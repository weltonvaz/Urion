#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Guilherme adora brincar com pipas, pipas de várias cores, formas e tamanhos. Ele tem percebido que para as pipas
possuírem maior estabilidade, e dessa forma voarem mais alto, elas devem possuir um barbante bem esticado ligando
todos os pares de pontas não vizinhas.'''

n = int(input())

print((n*(n-3))//2)