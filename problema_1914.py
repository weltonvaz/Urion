#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Amarelinha provavelmente é a brincadeira em que as crianças da vila mais se divertem, porém a mesma vem causando um bom tempo
   de discussão e choro nas crianças que a praticam. A causa do transtorno é para decidir quem será o próximo a pular, mas
   recentemente Quico (O gênio!) teve uma grande ideia para solucionar o problema. Basicamente a brincadeira só poderá ser jogada
   de dois em dois jogadores e para escolher o próximo jogador Quico indicou o uso do tradicional método par ou ímpar, onde os
   dois jogadores informam um número e se a soma desses números for par o jogador que escolheu PAR ganha ou vice verso. Entretanto
   a utilização desse método vem deixando o Quico louco, louco, louco... E por esse motivo ele pediu a sua ajuda! Solicitou a você
   um programa que dado o nome dos jogadores, suas respectivas escolhas PAR ou IMPAR e os números, informe quem foi o vencedor.'''

n = int(input())
for i in range(n):
    j = input().split()
    a = sum([int(x) for x in input().split()])
    print(j[j.index("PAR") - 1] if a%2 == 0 else j[j.index("IMPAR") - 1])