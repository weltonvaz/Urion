# -*- coding: utf-8 -*-

'''Todos os domingos, Bino faz compras na feira. Ele sempre marca com seu amigo Cino de se encontrarem
no terminal de ônibus da Parangaba às 8h, para irem juntos comprar na feira. Porém, muitas vezes Bino
acorda muito tarde e se atrasa para o encontro com seu amigo. Sabendo que Bino leva de 30 a 60 minutos
para chegar ao terminal. Diga o atraso máximo de Bino.'''

while True:
    try:
        h, m = (int(x) for x in input("").split(':'))
    except EOFError:
        break
    atraso = (m + 60*(h - 7) if h >= 7 else 0)
    print("Atraso maximo: %i" %atraso)