#!/usr/bin/env python3
# -*- coding: utf-8 

'''Como se sabe, existe um corvo com três olhos. O que não se sabia é que o corvo com três olhos pode
prever o resultado da loteria de Westeros. Enquanto todos os outros corvos coletam as apostas, o corvo
de três olhos já sabe o resultado, e quando Bran sonha com o corvo, o corvo conta o resultado. 
O problema é que Bran apesar de lembrar do sonho, não consegue interpretá-lo sozinho em tempo hábil. 
A sua tarefa é fazer um programa para interpretar o sonho de Bran e calcular o resultado da loteria.'''

for i in range(3):
    count = 0
    while True:
        wert = input("")
        if wert == "caw caw":
            print(count)
            break
        count += int(''.join('1' if x == '*' else '0' for x in wert), 2)
