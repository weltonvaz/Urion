import sys 
n = int(input())
e = []
d = 0
l = 0
h = []

x = input().split()
e = [int(y) for y in x]
h = [int(y) for y in x]

while True:
    if n == 0 or l >= n or l < 0:
        break
    if h[l] == e[l]:
        d += 1
    if h[l] == 0:
        break
    h[l] -= 1
    if h[l] % 2 == 0:
        l += 1
    else:
        l -= 1

print("%d %d" %(d, sum(h)))

