#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Mas ela não é muito boa em Matemática, e está solicitando sua ajuda para calcular a
porcentagem de que precisa para completar o cartaz.'''

a, b = (float(x) for x in input().split())
x = (((b*100)/a)-100)
print('{0:.2f}%'.format(x))