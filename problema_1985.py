# -*- coding: utf-8 -*-

'''O MacPRONALTS está com uma super promoção, exclusivo para os competidores da primeira Seletiva do
MaratonaTEC. Só que teve um problema, todos os maratonistas foram tentar comprar ao mesmo tempo, com
isso gerou uma fila muito grande. O pior é que a moça do caixa estava sem calculadora ou um programa
para ajudá-la a calcular com maior agilidade, eis que surge você para fazer um programa para ajudar a
coitada e aumentar a renda do MacPRONALTS. Segue o cardápio do dia contendo o número do produto e seu
respectivo valor.'''

produtos, total = {1001: 1.5, 1002: 2.5, 1003: 3.5, 1004: 4.5, 1005: 5.5}, 0
for i in range(int(input(""))):
    produto, quantidade = (int(x) for x in input("").split())
    total += produtos[produto]*quantidade
print("%.2f" %total)