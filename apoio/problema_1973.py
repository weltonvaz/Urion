import sys

def especial(lista[::-1]):
    i = 0
    while True:
        if lista[i] > 0:
            lista[i] = lista[i] - 1
            i += 1
        elif lista[i] == 0:
            break
    


estrelas = int(sys.stdin.readline())

carneiros = ''.join((str(y)for y in list(sys.stdin.readline()))).split()

roubados, visitadas, total, par, impar = 0, 0, 0, False, []

for e in carneiros:
    i = int(e)
    total += i
    visitadas += 1
    impar.append(i)
    if i % 2 == 0:
        par = True
        break
if par == True:
    especial(impar)
print(estrelas,visitadas)