n = int(input())
e = [int(x) for x in input().split()]

h = list(e)

d = 0
l = 0

while True:
    if n == 0 or l >= n or l < 0:
        break
    if h[l] == e[l]:
        d += 1
    if h[l] == 0:
        break
    h[l] -= 1
    if h[l] % 2 == 0:
        l += 1
    else:
        l -= 1

soma = sum(h)

print("%d %d" %(d, soma))
