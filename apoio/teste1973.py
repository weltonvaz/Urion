total, i, stolen, visited = sum(x), 0, 0, set()
while i in range(n) and x[i]:
    visited.add(i)
    stolen += 1
    x[i] -= 1
    if (x[i] + 1) % 2 == 1:
        i += 1
    else:
        i -= 1
print((len(visited)),(total - stolen))
