def fact(a):
    "just return a generator for the sequence you want"
    return (i for i in range(1,a+1) if a % i == 0)

num = 100

for b in fact(num):
    print(b)