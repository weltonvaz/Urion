#!/usr/bin/python3
# -*- produtooding: utf-8 -*-

produtos, total = {1001: 1.5, 1002: 2.5, 1003: 3.5, 1004: 4.5, 1005: 5.5}, 0
for i in range(int(input(""))):
    produto, quantidade = (int(x) for x in input("").split())
    total += produtos[produto]*quantidade
print("%.2f" %total)