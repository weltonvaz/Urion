#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Após comprar vários sítios adjacentes na região do oeste catarinense, a família
ovelha construiu uma única estrada que passa por todos os sítios em sequência.
O primeiro sítio da sequência foi batizado de ovelha 1, o segundo de ovelha 2, e
assim por diante. Porém, o irmão que vive em ovelha 1 acabou enlouquecendo e resolveu
fazer uma Jornada nas ovelhas para roubar carneiros das propriedades de seus irmãos.
Mas ele está definitivamente pirado. Quando passa pelo sítio ovelha i, ele rouba apenas
um carneiro daquele sítio (se o sítio tem algum) e segue ou para ovelha i + 1 ou para
ovelha i - 1, dependendo se o número de carneiros em ovelha i era, respectivamente,
ímpar ou par. Se não existe a ovelha para a qual ele deseja seguir, ele interrompe sua
jornada. O irmão louco começa sua Jornada em ovelha 1, roubando um carneiro do seu
próprio sítio.'''

n = int(input())
nums = input()
nums = map(int,nums.split())
pos,soma,total = 0,0,0
continua = True

for i,num in enumerate(nums):
    total+=num
    if(num%2==0 and continua):
        ataques = i+1
        soma+=((i*2)+1)-pos
        continua=False
    if(num-1==0 and continua):
        pos = i+1

else:
    if(soma>0):
        total-=soma
    else:
        ataques = n
        total-=ataques

print("%d %d"%(ataques,total))