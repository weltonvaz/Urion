#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''A ECI (Editio Chronica Incredibilis ou Editora de Crônicas Incríveis) é muito tradicional
   quando se trata de numerar as páginas de seus livros. Ela sempre usa a numeração romana 
   para isso. E seus livros nunca ultrapassam as 999 páginas pois, quando necessário, dividem
   o livro em volumes.
   Lembre que I representa 1, V é 5, X é 10, L é 50, C é 100, D é 500 e M representa 1000.'''
   
a = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"]
b = ["","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"]
c = ["","I","II","III","IV","V","VI","VII","VIII","IX"]

n = int(input())
x = divmod(n,100)
y = divmod(x[1],10)

res = a[n//100] + b[x[1]//10] + c[y[1]]

print(res)
