#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''Raul Seixas cantava que nasceu há 10 mil anos atrás e não tinha nada nesse mundo
 que ele não sabia demais. Os Mamomas Assassinas cantavam que mais de 10 mil anos
 "se passaram-se" [sic] quando eles repetiram a 5a série. Tantos eventos passados e
  o professor MC ficou curioso para saber em que ano tudo isso aconteceu.'''

for i in range(int(input())):
    y = 2015 - int(input())
    print("%i %s" %((y, "D.C.") if y > 0 else (y*(-1) + 1, "A.C.")))